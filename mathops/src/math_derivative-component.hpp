#ifndef OROCOS_MATH_DERIVATIVE_COMPONENT_HPP
#define OROCOS_MATH_DERIVATIVE_COMPONENT_HPP

#include <rtt/RTT.hpp>
#include <rtt/os/TimeService.hpp>
#include <rtt/Time.hpp>
#include <rosmathops/Value.h>

class Math_derivative : public RTT::TaskContext{
  public:
    Math_derivative(std::string const& name);
    bool configureHook();
    bool startHook();
    void updateHook();
    void stopHook();
    void cleanupHook();
  private:
    RTT::OutputPort<rosmathops::Value> _outPort;
    RTT::InputPort<rosmathops::Value> _evPort;
    RTT::os::TimeService::ticks LastMoment;
    double LastValue;
    int FirstTime;
};
#endif
