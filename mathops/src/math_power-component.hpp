#ifndef OROCOS_MATH_POWER_COMPONENT_HPP
#define OROCOS_MATH_POWER_COMPONENT_HPP

#include <rtt/RTT.hpp>
#include <rosmathops/Value.h>

class Math_power : public RTT::TaskContext{
  public:
    Math_power(std::string const& name);
    bool configureHook();
    bool startHook();
    void updateHook();
    void stopHook();
    void cleanupHook();
  private:
    RTT::InputPort<rosmathops::Value> _evPort;
    RTT::OutputPort<rosmathops::Value> _outPort;
};
#endif
