#ifndef OROCOS_MATH_AMPLIFICATION_COMPONENT_HPP
#define OROCOS_MATH_AMPLIFICATION_COMPONENT_HPP

#include <rtt/RTT.hpp>
#include <rosmathops/Value.h>

class Math_amplification : public RTT::TaskContext{
  public:
    Math_amplification(std::string const& name);
    bool configureHook();
    bool startHook();
    void updateHook();
    void stopHook();
    void cleanupHook();
  private:
    RTT::InputPort<rosmathops::Value> _evPort;
    RTT::OutputPort<rosmathops::Value> _outPort;
    double k;
    void ChangeCoefficient(double in);
};
#endif
