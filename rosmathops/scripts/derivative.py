#!/usr/bin/env python

import rospy
from rosmathops.msg import Value



def callback(data):
    #now=rospy.get_time()
    global PrevValue
    global PrevTime
    _Value.val=(data.val-PrevValue)*10
    pub.publish(_Value)
    #pub.publish((data.data-PrevValue)/(now-PrevTime))
    PrevValue=data.val
    #PrevTime=now

_Value=Value()
PrevValue=0
PrevTime=0
pub = rospy.Publisher('OUT', Value, queue_size=10)

def derivative():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'talker' node so that multiple talkers can
    # run simultaneously.
    rospy.init_node('derivative', anonymous=True)
    PrevTime=rospy.get_time()
    PrevValue=0
    rospy.Subscriber("IN", Value, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    derivative()
