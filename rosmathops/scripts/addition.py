#!/usr/bin/env python

import rospy
from rosmathops.msg import Value

def callback1(data):
    global _Value
    _Value.val=data.val+input2
    pub.publish(_Value)
def callback2(data):
    global input2
    input2=data.val

_Value=Value()
input2=0

pub = rospy.Publisher('OUT', Value, queue_size=10)

def addition():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'talker' node so that multiple talkers canXzc.34el5
    # run simultaneously.
    rospy.init_node('addition', anonymous=True)
    rospy.Subscriber("IN1", Value, callback1)
    rospy.Subscriber("IN2", Value, callback2)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    addition()
