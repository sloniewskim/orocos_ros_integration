#!/usr/bin/env python

import rospy
from rosmathops.msg import Value

def callback(data):
    global _Value
    _Value.val=abs(data.val)
    pub.publish(_Value)


pub = rospy.Publisher('OUT', Value, queue_size=10)

_Value=Value()

def absolute():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'talker' node so that multiple talkers canXzc.34el5
    # run simultaneously.
    rospy.init_node('absolute', anonymous=True)

    rospy.Subscriber("IN", Value, callback)
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    absolute()
