#!/usr/bin/env python

import rospy
from rosmathops.msg import Value
import sys


def callback(data):
    #now=rospy.get_time()
    global Sum
    global _Value
    #global PrevTime
    Sum+=data.val/10
    _Value=Sum
    pub.publish(Sum)
    #PrevTime=now

_Value=Value()
Sum=0
PrevTime=0
pub = rospy.Publisher('OUT', Value, queue_size=10)

def integration():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'talker' node so that multiple talkers can
    # run simultaneously.
    global Sum
    Sum=float(sys.argv[1])
    rospy.init_node('integration', anonymous=True)
    PrevTime=rospy.get_time()
    PrevValue=0
    rospy.Subscriber("IN", Value, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    integration()
