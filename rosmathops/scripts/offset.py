#!/usr/bin/env python

import rospy
from rosmathops.msg import Value
import sys

def callback(data):
    _Value.val=data.val+offset
    pub.publish(_Value)

_Value=Value()
offset=1

pub = rospy.Publisher('OUT', Value, queue_size=10)

def offest():
    global offset
    offset=float(sys.argv[1])
    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'talker' node so that multiple talkers canXzc.34el5
    # run simultaneously.
    rospy.init_node('offset', anonymous=True)

    rospy.Subscriber("IN", Value, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    offest()
