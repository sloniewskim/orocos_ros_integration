#!/usr/bin/env python


import rospy
from rosmathops.msg import Value
import math
import sys
def sine():
    _Value=Value()
    omega=float(sys.argv[1])
    phase=float(sys.argv[2])
    pub = rospy.Publisher('OUT', Value, queue_size=10)
    rospy.init_node('sine', anonymous=True)
    rate = rospy.Rate(30) # 10hz
    while not rospy.is_shutdown():
        seconds = rospy.get_time()
        _Value.val = math.sin(seconds*omega+phase)
        #rospy.loginfo(_Value.val)
        pub.publish(_Value)
        rate.sleep()

if __name__ == '__main__':
    try:
        sine()
    except rospy.ROSInterruptException:
        pass
